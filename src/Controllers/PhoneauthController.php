<?php
/**
 * Created by PhpStorm.
 * User: ihsanberahim
 * Date: 21/07/2019
 * Time: 4:45 PM
 */

namespace GoalDriven\WpFP\Contollers;


use Exception;
use Firebase\Auth\Token\Exception\InvalidToken;
use GoalDriven\WpFP\Core;

class PhoneauthController {
	/**
	 * @param \Illuminate\Http\Request $request
	 *
	 * @return mixed
	 */
	public function create( $request ) {
		$request->merge( [
			'post_title' => 'Phone Auth',
			'overrides'  => []
		] );

		return wpfp_view()->make( 'phone-auth' );
	}

	/**
	 * @param \Illuminate\Http\Request $request
	 */
	public function store( $request ) {
		$v = gd_validator()->make(
			$request->all(),
			[
				'accessToken' => 'required',
				'phoneNumber' => 'required'
			]
		);

		if ( $v->fails() ) {
			wp_die( 'Access token and phone number is required' );
		}

		$accessToken       = $request->input( 'accessToken' );
		$phone_number      = $request->input( 'phoneNumber' );
		$the_user_meta_key = Core::config_phone_auth()->get( 'user_meta_key' );
		$user              = $request->user;

		Core::firebase()->getAuth()->verifyIdToken( $accessToken );

		update_user_meta( $user->ID, $the_user_meta_key, $phone_number );

		return [ 'Success' ];
	}
}
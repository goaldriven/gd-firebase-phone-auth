<?php
/**
 * Created by PhpStorm.
 * User: ihsanberahim
 * Date: 21/07/2019
 * Time: 7:30 AM
 */


if( !function_exists('wpfp_comp') ) {
	function wpfp_comp() {
		// return gd_new_comp( __FILE__ );
		return gd_new_comp( WP_CONTENT_DIR . '/plugins/gd-firebase-phone-auth/gd-firebase-phone-auth.php' );
	}
}

if( !function_exists('wpfp_view') ) {
	function wpfp_view() {
		return gd_view_factory( GDWPFP_DIR . '/resources/views', WP_CONTENT_DIR );
	}
}

<?php

namespace GoalDriven\WpFP;

use GoalDriven\Supports\Services\Router;
use Illuminate\Support\Str;
use Kreait\Firebase\Factory;
use Kreait\Firebase\ServiceAccount;

defined( 'ABSPATH' ) or die( '' );

class Core {
	public static function init() {
		add_action( 'wp_enqueue_scripts', [ self::class, 'enqueue_scripts' ] );

		self::loadWebRoutes();
		self::loadApiRoutes();

		self::forceTargetedRolesVerifyPhonenumber();

		Router::run();
	}

	public static function forceTargetedRolesVerifyPhonenumber() {
		$targeted_uri = Router::cleanUri( self::config_phone_auth()->get( 'uri' ) );
		$current_uri  = Router::cleanUri( Router::httpMeta()->get( 'uri' ) );

		if ( ! is_admin() && ! self::isApi() && $current_uri !== $targeted_uri ) {
			if ( self::validateTargetedRoles()
			     && empty( self::getUserMetaKeyValue() )
			) {
				wp_redirect( home_url( $targeted_uri . '/' ) );
				exit;
			}
		}

	}

	// Check if any of the current user roles match the targeted roles
	public static function validateTargetedRoles() {
		$phone_auth_config = self::config_phone_auth();
		$user              = wp_get_current_user();

		$result = collect( $user->roles )->filter( function ( $v ) use ( $phone_auth_config ) {
			return in_array( $v, $phone_auth_config->get( 'default_roles' ) );
		} )->first();

		return $result !== null ? true : false;
	}

	// Check if the user has value for the required meta key
	public static function validateUserMetaKey() {
		$user_meta_value = self::getUserMetaKeyValue();

		return ! empty( $user_meta_value ) ? true : false;
	}


	public static function getUserMetaKeyValue() {
		$phone_auth_config = self::config_phone_auth();
		$user              = wp_get_current_user();
		$user_meta_value   = get_user_meta( $user->ID, $phone_auth_config->get( 'user_meta_key' ), true );

		return $user_meta_value;
	}


	public static function loadWebRoutes() {
		Router::loadRouteFromFile( GDWPFP_DIR . '/routes/web.php' );
	}

	public static function isApi() {
		$current_uri = Router::httpMeta()->get( 'uri' );

		return Str::is( '/api*', $current_uri );
	}

	public static function loadApiRoutes() {
		Router::loadRouteFromFile( GDWPFP_DIR . '/routes/api.php' );
	}


	public static function config_firebase() {
		return gd_config( GDWPFP_DIR . '/config/firebase.php' );
	}


	public static function config_phone_auth() {
		return gd_config( GDWPFP_DIR . '/config/phone_auth.php' );
	}

	public static function firebase() {
		$firebase_config = self::config_firebase();

		$serviceAccount = ServiceAccount::fromJson(
			$firebase_config->get( 'service_account' )
		);

		return ( new Factory )
			->withServiceAccount( $serviceAccount )
			->create();
	}

	public static function enqueue_scripts() {
		$plugin_url = wpfp_comp()->wp_get_resource_uri();

		wp_enqueue_script( 'firebasejs-app', 'https://www.gstatic.com/firebasejs/6.3.1/firebase-app.js', [], '6.3.1', true );
		wp_enqueue_script( 'firebasejs-auth', 'https://www.gstatic.com/firebasejs/6.3.1/firebase-auth.js', [], '6.3.1', true );
		wp_enqueue_script( 'firebasejs-firestore', 'https://www.gstatic.com/firebasejs/6.3.1/firebase-firestore.js', [], '6.3.1', true );
		wp_enqueue_script( 'sweetalert2', $plugin_url . '/node_modules/sweetalert2/dist/sweetalert2.all.min.js', [], '8.14.0', true );
		wp_enqueue_script( 'axios', $plugin_url . '/node_modules/axios/dist/axios.js', [], '0.19.0', true );
		wp_enqueue_script( 'gdwpfp-utils', $plugin_url . '/public/scripts/utils.js', [], '1.0.0', true );

		// Register the script
		wp_register_script( 'gdwpfp-http_client', $plugin_url . '/public/scripts/http-client.js', [
			'axios',
			'gdwpfp-utils'
		], '1.0.0', true );

		wp_register_script( 'gdwpfp-phone_auth_endpoint', $plugin_url . '/public/scripts/phone-auth-endpoint.js', [
			'gdwpfp-http_client',
		], '1.0.0', true );

		wp_register_script( 'gdwpfp-phone_auth', $plugin_url . '/public/scripts/phone-auth.js', [
			'jquery',
			'firebasejs-app',
			'firebasejs-auth',
			'sweetalert2',
			'gdwpfp-http_client',
			'gdwpfp-phone_auth_endpoint'
		], '1.0.0', true );

		// Localize the script with new data
		$config_firebase = self::config_firebase();

		wp_localize_script( 'gdwpfp-phone_auth', 'firebaseConfig', $config_firebase->get( 'config' ) );


		$gdwpfp = [
			'baseURL' => home_url( '/' )
		];

		wp_localize_script( 'gdwpfp-http_client', 'GDWPFP', $gdwpfp );

		// Enqueued script with localized data.
		wp_enqueue_script( 'gdwpfp-phone_auth' );
		wp_enqueue_script( 'gdwpfp-http_client' );
		wp_enqueue_script( 'gdwpfp-phone_auth_endpoint' );
	}
}
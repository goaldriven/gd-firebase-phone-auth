class GdWpFp {
    instance;

    api() {
        this.instance = axios.create( {
            baseURL: GDWPFP.baseURL,
            timeout: 3000
        } );

        this.instance.interceptors.response.use(
            this.onResponseSuccess,
            this.onResponseFail
        );

        this.instance.interceptors.request.use( this.onRequest, this.onRequestError );

        return this.instance;
    }

    onRequest( config ) {

        if ( typeof config.data === 'object' ) {
            Object.assign( config, {
                data: objectToFormData( config.data )
            } );
        }

        return config;
    }

    onRequestError( error ) {
        return Promise.reject( error.code );
    }

    onResponseSuccess( response ) {
        return Promise.resolve( response );
    }

    onResponseFail( error ) {
        console.log( [ 'GdWpFp.onResponseFail', error, GDWPFP ] );

        if ( error.response ) {
            if ( error.response.status === 401 ) {
                // do something on unauthorized request
            }
        }

        return Promise.reject( error.code );
    }
}

const gdwpfpApi = () => {
    return ( new GdWpFp() ).api();
};
class PhoneauthEndpoint {
    static store( firebase_user ) {
        const firebaseUser = JSON.parse( JSON.stringify( firebase_user ) );
        const { phoneNumber, stsTokenManager } = firebaseUser;
        const { accessToken } = stsTokenManager;

        return gdwpfpApi().post( '/api/phone-auth', { accessToken, phoneNumber } );
    }
}
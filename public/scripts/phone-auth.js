jQuery( document ).ready( ( $ ) => {

    const recaptchaContainerId = 'recaptcha-container';
    const phoneAuthFormId = '#phone_auth_form';
    const phoneNumberId = '#phone_number';
    const codeId = '#code';

    if ( !$( phoneAuthFormId )[ 0 ] ) return; // do nothing if the form not there

    // Initialize Firebase
    firebase.initializeApp( firebaseConfig );

    window.recaptchaVerifier = new firebase.auth.RecaptchaVerifier( recaptchaContainerId, {
        'size': 'normal',
        'callback': function ( response ) {
            // reCAPTCHA solved, allow signInWithPhoneNumber.
            // ...
        },
        'expired-callback': function () {
            // Response expired. Ask user to solve reCAPTCHA again.
            Swal.fire( {
                title: 'Opps..',
                text: 'Refresh page or just resolve the reCAPTCHA again.',
                type: 'error',
                confirmButtonText: 'Ok',
                onClose: () => {
                    window.location.reload();
                }
            } );
        }
    } );

    window.recaptchaVerifier.render().then( function ( widgetId ) {
        window.recaptchaWidgetId = widgetId;
    } );

    $( phoneAuthFormId ).on( 'submit', function ( e ) {
        e.preventDefault();

        let phoneNumber = $( phoneNumberId, phoneAuthFormId ).val();
        let code = $( codeId, phoneAuthFormId ).val();
        let appVerifier = window.recaptchaVerifier;

        if ( phoneNumber[ 0 ] === '0' ) {
            phoneNumber = parseInt( phoneNumber );
            phoneNumber = `60${phoneNumber}`;

            // Apply Malaysia country code
            // Override the number to let the user know
            $( phoneNumberId, phoneAuthFormId ).val( phoneNumber );
        }

        // Force phone number to have + sign
        if ( phoneNumber[ 0 ] !== '+' ) {
            phoneNumber = `+${phoneNumber}`;
        }

        if ( window.confirmationResult && code !== '' ) {
            confirmationResult.confirm( code )
                .then( r => {
                    PhoneauthEndpoint.store( r.user )
                        .then( () => {
                            window.location.href = GDWPFP.baseURL;
                        } )
                        .catch( e => {
                            // Alert user the error
                            Swal.fire( {
                                title: 'Opps..',
                                text: e.message,
                                type: 'error',
                                confirmButtonText: 'Ok'
                            } );
                        } );
                } ).catch( e => {

                // Alert user the error
                Swal.fire( {
                    title: 'Opps..',
                    text: e.message,
                    type: 'error',
                    confirmButtonText: 'Ok'
                } );

            } );
        } else if ( phoneNumber !== '' ) {
            firebase.auth().signInWithPhoneNumber( phoneNumber, appVerifier )
                .then( function ( confirmationResult ) {
                    // SMS sent. Prompt user to type the code from the message, then sign the
                    // user in with confirmationResult.confirm(code).
                    window.confirmationResult = confirmationResult;

                    // Lock phone input
                    $( phoneNumberId, phoneAuthFormId ).attr( 'readonly', true );

                    // Show code input
                    $( codeId, phoneAuthFormId ).parent().show();

                    // Enable code input
                    $( codeId, phoneAuthFormId ).attr( 'disabled', false );

                    // Fochs code input
                    $( codeId, phoneAuthFormId ).focus();

                    // Hide recaptcha
                    $( `#${recaptchaContainerId}` ).hide();
                } ).catch( function ( e ) {
                // Error; SMS not sent
                // ...
                // Unlock phone input
                $( phoneNumberId, phoneAuthFormId ).attr( 'readonly', false );

                // Focus phone input
                $( phoneNumberId, phoneAuthFormId ).focus();

                // Hide code input
                $( codeId, phoneAuthFormId ).parent().hide();

                // Disable code input
                $( codeId, phoneAuthFormId ).attr( 'disabled', true );

                // Reset confirmation result
                window.confirmationResult = null;

                // Show recaptcha
                $( `#${recaptchaContainerId}` ).show();

                // Override error message
                switch ( e.code ) {
                    case 'auth/invalid-phone-number':
                        Object.assign( e, {
                            message: 'Invalid phone number'
                        } );
                        break;
                }

                // Alert user the error
                Swal.fire( {
                    type: 'error',
                    title: 'Opps..',
                    text: e.message,
                    confirmButtonText: 'Ok'
                } );
            } );
        } else {

        }

    } );
} );
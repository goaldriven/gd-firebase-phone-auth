<form id="phone_auth_form">
    <label for="phone_number">Phone Number:
        <small>(Example: 0123456789 or 60123456789)</small>
        <input id="phone_number" name="phone_number" type="tel" class="form-control" required>
    </label>
    <label for="code" style="display: none;">Code:
        <input id="code" name="code" type="number" required disabled="disabled">
    </label>
    <div id="recaptcha-container"></div>
    <button>Continue</button>
</form>
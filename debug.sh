#!/usr/bin/env bash

WP_HOME=/Users/ihsanberahim/Documents/Wordpress_Workspace/wpmu-playground
PLUGIN_NAME=gd-firebase-phone-auth

wp plugin deactive $PLUGIN_NAME --force --path=$WP_HOME
wp plugin uninstall $PLUGIN_NAME --force --path=$WP_HOME
ln -s $(pwd)/ $WP_HOME/wp-content/plugins/$PLUGIN_NAME
wp plugin activate $PLUGIN_NAME --path=$WP_HOME
echo "Done."
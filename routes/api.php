<?php defined( 'ABSPATH' ) or die( '' );

/**
 * Api Routes.
 */

use FastRoute\RouteCollector;
use GoalDriven\Supports\Services\Router;
use \GoalDriven\WpFP\Contollers\PhoneauthController;

defined( 'ABSPATH' ) or die( '' );

Router::register()->addGroup( '/api', function ( RouteCollector $r ) {
	$r->addRoute( 'POST', '/phone-auth', [ PhoneauthController::class, 'store' ] );
} );
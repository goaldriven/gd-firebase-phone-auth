<?php

/**
 * Web Routes.
 */

use FastRoute\RouteCollector;
use GoalDriven\Supports\Services\Router;
use GoalDriven\WpFP\Contollers\PhoneauthController;

defined( 'ABSPATH' ) or die( '' );

Router::register()->addGroup( '/', function ( RouteCollector $r ) {
	$r->addRoute( 'GET', 'phone-auth', [ PhoneauthController::class, 'create' ] );
} );
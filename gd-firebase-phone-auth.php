<?php
/**
 * Plugin Name:     Firebase Phone Auth
 * Plugin URI:      https://bitbucket.org/goaldriven/gd-firebase-phone-auth
 * Description:     Enable Phone Auth for Wordpress
 * Author:          GoalDriven
 * Author URI:      https://goaldriven.co?s=Gd+Firebase+Phone+Auth
 * Text Domain:     gd-firebase-phone-auth
 * Domain Path:     /languages
 * Version:         1.0.5
 *
 * @package         GoalDriven_Wordpress_Firebase_Phoneauth
 */

try {
	require_once( __DIR__ . '/vendor/autoload.php' );
} catch ( Exception $e ) {
	return;
}

define( 'GDWPFP_DIR', __DIR__ );

add_action( 'setup_theme', function () {
	if ( ! defined( 'GDWPS_ACTIVE' ) ) {
		add_action( 'admin_notices', function () {
			?>
            <div class="notice notice-error is-dismissible">
                <p><?php _e( 'Gd Wordpress Support required and should be activated first.' ); ?></p>
            </div>
			<?php
		} );

		return;
	}

	/**
	 * Load Plugin
	 */
	add_action( 'after_setup_theme', [ \GoalDriven\WpFP\Core::class, 'init' ] );


	/**
	 * Enable version checker
	 */
	if( $checker = gd_setup_plugin_update_checker( __FILE__ ) ) {
	    $checker->setBranch('master');
    }
} );
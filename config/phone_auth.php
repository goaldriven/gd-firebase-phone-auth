<?php defined( 'ABSPATH' ) or die( '' );

return [
	'uri'           => immutable( 'PHONE_AUTH_URI', '/phone-auth' ),
	'default_roles' => explode(',',
		immutable( 'ROLES_TO_FORCE_PHONE_AUTH', [
			// Get main site default_role
			get_blog_option(
				immutable('BLOG_ID_CURRENT_SITE', 1),
				'default_role' )
		] )
	),
	'user_meta_key' => immutable( 'USER_META_KEY_FOR_PHONE_AUTH', 'verified_phone_number' )
];
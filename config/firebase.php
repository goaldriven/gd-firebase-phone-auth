<?php defined( 'ABSPATH' ) or die( '' );

return [
	'config' => json_decode(
		immutable('FIREBASE_CONFIG', '{}'),
		true
	),
	'service_account' => immutable('FIREBASE_SERVICE_ACCOUNT', '{}')
];